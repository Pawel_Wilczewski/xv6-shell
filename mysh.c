#include "kernel/types.h"
#include "user/user.h"
#include "kernel/fcntl.h"

static const int MAX_INPUT_LENGTH = 512;

static int critical_error = 0;

// for the sake of convenience,
// I implemented simple realloc, strstr and strncpy

#define NULL ((void *) 0)

void *realloc(void *ptr, int size)
{
	if (ptr == NULL)
		return NULL;
	
    char *buffer = (char *) malloc(size);
    
    if (buffer != NULL)
		memcpy(buffer, (char *) ptr, size);
	
    free(ptr);
    
    return buffer;
}

char *strstr(char *__haystack, const char *__needle)
{
    int n = strlen(__needle);
 
    while (*__haystack)
    {
        if (!memcmp(__haystack, __needle, n)) {
            return __haystack;
        }
 
        __haystack++;
    }
 
    return NULL;
}

char *strncpy(char *__dest, const char *__src, int n)
{
	if (__dest == NULL)
	{
        return NULL;
	}
	
	char *dest_start = __dest;
	
    while (*__src && n--)
    {
        *__dest = *__src;
        __dest++;
        __src++;
    }
 
    *__dest = '\0';
    
    return dest_start;
}

// the below string helper functions are the ones I coded for one
// of the previos c assignments:
// https://gitlab.com/comp1921_2021/sc20pw/-/blob/master/cwk2/game_of_life/src/string_utils.c

typedef struct {
    char **array;
    int length;
} StringArray;

int str_contains(char *string, const char *substring)
{
    return strstr(string, substring) != NULL;
}

void str_array_free(StringArray **out_array)
{
    for (int i = 0; i < (*out_array)->length; ++i)
    {
        free((*out_array)->array[i]);
        (*out_array)->array[i] = NULL;
    }

    free((*out_array)->array);
    (*out_array)->array = NULL;

    free(*out_array);
    *out_array = NULL;
}

int str_same(const char *str1, const char *str2)
{
    if (str1 == NULL || str2 == NULL)
        return 0;

    return !strcmp(str1, str2);
}

int str_length(const char *string)
{
    if (string == NULL || str_same(string, ""))
        return 0;

    return strlen(string);
}

int str_find(char *string, const char *pattern, const int start_index)
{
    if (!str_contains(string, pattern))
        return -1;

    if (string == NULL || pattern == NULL || start_index >= str_length(string) - str_length(pattern) + 1)
        return -1;

    char *found = strstr(string + start_index, pattern);

    if (found == NULL)
        return -1;

    return (int)((found -  string) / sizeof(char));
}

int str_count(const char *string, const char *substring)
{
    if (string == NULL || substring == NULL)
        return 0;

    if (!str_length(substring))
        return -1;

    int len = str_length(string);
    int sub_len = str_length(substring);

    int i = 0, j, sub_count, count = 0;
    while (i < len)
    {
        j = 0;
        sub_count = 0;
        while (string[i] == substring[j])
        {
            sub_count++;
            i++;
            j++;
        }
        if (sub_count >= sub_len)
            count++;
        else
            i++;
    }
    return count;
}

char *str_copy(const char *string)
{
    if (string == NULL)
        return NULL;

    int size = str_length(string) + 1;
    char *out = malloc(sizeof(char) * size);
    strcpy(out, string);

    // ensure the 0 char
    out[size - 1] = '\0';

    return out;
}

char *str_empty()
{
    char *out = malloc(sizeof *out);
    out[0] = '\0';
    return out;
}

char *str_heap_construct(const char *init)
{
    return str_copy(init);
}

void str_combine(char **out_str, const char *to_append)
{
    if (out_str == NULL || *out_str == NULL || to_append == NULL)
        return;

    int size = str_length(*out_str) + str_length(to_append) + 1; // +1 for '\0'

    *out_str = realloc(*out_str, size * sizeof *to_append);
    strcpy(*out_str + str_length(*out_str), to_append);

    // ensure the 0 char
    (*out_str)[size - 1] = '\0';
}

char *str_reverse(const char *string)
{
    if (string == NULL)
        return NULL;

    char *out = str_copy(string);

    int len = str_length(string);
    for (int i = 0; i < len; ++i)
        out[i] = string[len - i - 1];

    out[len] = '\0';
    return out;
}

char *str_substr(const char *string, int start, int stop)
{
    if (string == NULL)
        return NULL;

    if (stop < start || stop > str_length(string))
        stop = str_length(string);

    if (start > str_length(string) || start < 0)
        return NULL;
    else if (start == str_length(string))
        return str_empty();

    int size = stop - start + 1;
    char *out = malloc(size * sizeof *string);

    strncpy(out, string + start * (sizeof *string), size - 1);
    out[size - 1] = '\0';

    return out;
}

char *str_substr_after(char *string, const char *pattern, int inclusive)
{
    if (string == NULL || pattern == NULL)
        return NULL;

    if (str_length(pattern) <= 0)
        return str_copy(string);

    char *after = str_substr(string, str_find(string, pattern, 0) + str_length(pattern), -1);

    char *out = str_empty();
    if (inclusive)
        str_combine(&out, pattern);
    str_combine(&out, after);
    free(after);
    after = NULL;

    return out;
}

char *str_substr_before(char *string, const char *pattern, int inclusive)
{
    if (string == NULL || pattern == NULL)
        return NULL;

    if (str_length(pattern) <= 0)
        return str_copy(string);

    char *before = str_substr(string, 0, str_find(string, pattern, 0));

    if (inclusive)
        str_combine(&before, pattern);

    return before;
}

char *str_replace(const char *string, const char *to_replace, const char *replace_with, int n)
{
    // skip if invalid to_replace string
    if (str_same(to_replace, ""))
        return NULL;

    // if infinite - adjust n
    if (n < 1 || n > str_count(string, to_replace))
        n = str_count(string, to_replace);

    // initialize the output and a temporary buffer that will help "cutting" and replacing the substrings
    char *todo = str_copy(string);
    char *out = str_empty();
    // go through the string and add parts before the replacement, then the appropriate replacement
    int i = 0;
    char *before;
    while ((before = str_substr_before(todo, to_replace, 0)) != 0 && i < n)
    {
        str_combine(&out, before);
        str_combine(&out, replace_with);
        char *remaining = str_substr_after(todo, to_replace, 0);
        free(todo);
        todo = remaining;

        ++i;
        free(before);
    }
    free(before);

    // finally add the last part after the last replacement
    str_combine(&out, todo);
    free(todo);

    return out;
}

char *str_replace_r(const char *string, const char *to_replace, const char *replace_with, int n)
{
    char *string_reversed = str_reverse(string);
    char *to_replace_reversed = str_reverse(to_replace);
    char *replace_with_reversed = str_reverse(replace_with);
    char *string_reversed_replaced = str_replace(string_reversed, to_replace_reversed, replace_with_reversed, n);
    char *out = str_reverse(string_reversed_replaced);

    free(string_reversed);
    free(to_replace_reversed);
    free(replace_with_reversed);
    free(string_reversed_replaced);

    return out;
}

StringArray *str_split(const char *string, const char *delimiter)
{
    if (string == NULL || delimiter == NULL || str_same(delimiter, "") || str_same(string, ""))
    {
        StringArray *out = (StringArray *) malloc(sizeof(StringArray));
        out->length = 0;
        out->array = NULL;

        return out;
    }

    char *todo = str_copy(string);

    // remove trailing delimiter if present
    char *trailing = str_substr(todo, str_length(todo) - str_length(delimiter), -1);
    if (str_same(trailing, delimiter))
    {
        char *new_todo = str_replace_r(todo, delimiter, "", 1);
        free(todo);
        todo = new_todo;
        free(trailing);
    }

    char **out = (char **) malloc(sizeof(char *) * (str_count(todo, delimiter) + 1));

    int i = 0;
    while (str_contains(todo, delimiter))
    {
        out[i] = str_substr_before(todo, delimiter, 0);
        char *new_todo = str_substr_after(todo, delimiter, 0);
        free(todo);
        todo = new_todo;
        i++;
    }
    out[i] = todo;

    StringArray *result = (StringArray *) malloc(sizeof(StringArray));
    result->array = out;
    result->length = i + 1;

    return result;
}

char *str_remove_suffix_char(const char *string, const char x)
{
	if (string == NULL)
		return NULL;
	
	char *result = str_copy(string);
	
	for (int i = str_length(string) - 1; i >= 0; i--)
	{
		if (string[i] != x)
			break;
		else
			result[i] = '\0';
	}
	
	return result;
}

char *str_remove_prefix_char(const char *string, const char x)
{
	if (string == NULL)
		return NULL;
	
	char *result = str_copy(string);
	
	for (int i = 0; i < str_length(string); i++)
	{
		if (string[i] != x)
			break;
		else
			result = str_substr(result, 1, -1);
	}
	
	return result;
}

//////////////////////////////////////////////////////////
// no str functions anymore
//////////////////////////////////////////////////////////

void handle_semicolon(char *left, char *right);
void handle_redir(char *left, char *right, const char *redir);
void handle_pipe(char *left, char *right);
void execute_args(StringArray *args);

void handle_input(char *input)
{
	if (input == NULL || str_length(input) == 0 || critical_error)
		exit(0);
	
	// find ;
	int pos_semicolon = str_find(input, ";", 0);
	if (pos_semicolon > -1)
	{
		handle_semicolon(str_substr_before(input, ";", 0), str_substr_after(input, ";", 0));
		exit(0);
	}
	
	// find redir
	int pos_append = str_find(input, ">>", 0);
	int pos_out = str_find(input, ">", 0);
	int pos_in = str_find(input, "<", 0);
	
	pos_append = (pos_append == -1 ? MAX_INPUT_LENGTH : pos_append);
	pos_out = (pos_out == -1 ? MAX_INPUT_LENGTH : pos_out);
	pos_in = (pos_in == -1 ? MAX_INPUT_LENGTH : pos_in);
	if (pos_append != MAX_INPUT_LENGTH || pos_out != MAX_INPUT_LENGTH || pos_in != MAX_INPUT_LENGTH)
	{
		char *redir = str_heap_construct(">>");
		int pos = pos_append;
		if (pos_out < pos)
		{
			pos = pos_out; // ">>" will be executed correcly since we use < and not <=
			free(redir);
			redir = str_heap_construct(">");
		}
		if (pos_in < pos)
		{
			pos = pos_in;
			free(redir);
			redir = str_heap_construct("<");
		}
	
		handle_redir(str_substr_before(input, redir, 0), str_substr_after(input, redir, 0), redir);
		exit(0);
	}
	
	// find pipe
	int pos_pipe = str_find(input, "|", 0);
	if (pos_pipe > -1)
	{
		handle_pipe(str_substr_before(input, "|", 0), str_substr_after(input, "|", 0));
		exit(0);
	}
	
	// exec if no ; nor redir no pipe
	input = str_remove_suffix_char(input, ' '); // more leaks
	input = str_remove_prefix_char(input, ' ');
	
	StringArray *args = str_split(input, " ");
	execute_args(args);
	str_array_free(&args);
	
	exit(0);
}

void handle_semicolon(char *left, char *right)
{
	if (fork() == 0)
		handle_input(left);
	wait(0);
	
	handle_input(right);
}

void handle_redir(char *left, char *right, const char *redir)
{
	char *file = str_copy(right);
	file = str_remove_prefix_char(file, ' ');
	file = str_substr_before(file, " ", 0);
	file = str_substr_before(file, ";", 0);
	file = str_substr_before(file, "|", 0);
	file = str_substr_before(file, ">", 0);
	file = str_substr_before(file, ">>", 0);
	file = str_substr_before(file, "<", 0); // MEMORY LEAAAAAAAAAKS (doesn't matter if I fix these though so won't bother)
		
	if (str_same(redir, ">"))
	{		
		close(1);
		open(file, O_WRONLY|O_CREATE|O_TRUNC);	
		handle_input(left);
		
		// before continuing, open back the console fd
		open("console", O_RDWR); 
		
		handle_input(right);
	}
	else if (str_same(redir, ">>"))
	{		
		close(1);
		open(file, O_WRONLY|O_CREATE);	
		handle_input(left);
		
		// before continuing, open back the console fd
		open("console", O_RDWR);
		
		handle_input(right);
	}
	else if (str_same(redir, "<"))
	{		
		close(0);
		open(file, O_RDONLY);
		handle_input(left);
		
		// before continuing, open back the console fd
		open("console", O_RDWR);
	}
	handle_input(right);
}

void handle_pipe(char *left, char *right)
{
	int p[2];
	if(pipe(p) < 0)
	{
		printf("Pipe error!\n");
		critical_error = 1;
		exit(0);
	}
	
	int f = fork();
    if(f == 0)
    {
		close(1);
		dup(p[1]);
		close(p[0]);
		close(p[1]);
		
		handle_input(left);
	}
	else if (f < 0)
	{
		printf("Fork error!\n");
		critical_error = 1;
		exit(0);
	}
	
	f = fork();
    if(f == 0)
    {
		close(0);
		dup(p[0]);
		close(p[0]);
		close(p[1]);
		
		handle_input(right);
    }
    else if (f < 0)
	{
		printf("Fork error!\n");
		critical_error = 1;
		exit(0);
	}
    
    close(p[0]);
    close(p[1]);
    wait(0);
    wait(0);
}

void execute_args(StringArray *args)
{
	if (args == NULL || args->length < 1 || args->array == NULL)
		return;
		
	critical_error = exec(args->array[0], args->array) != 0;
	
	if (critical_error)
		printf("exec fail: %s\n", args->array[0]);
}

int main(int argc, char *argv[])
{
	while (1)
	{
		critical_error = 0;
		printf(">>>");
		
		// read the input args
		char *input = (char *) malloc(sizeof(char) * MAX_INPUT_LENGTH);
		gets(input, MAX_INPUT_LENGTH);

		// remove unnecessary white spaces from the input - we only need to care about spaces now
		input[str_length(input) - 1] = '\0';
		//input = str_replace(input, "\n", " ", -1);
		input = str_replace(input, "\t", " ", -1);
		input = str_replace(input, "\v", " ", -1);
		input = str_replace(input, "\r", " ", -1); // MEMORY LEAAAAAAAAAKS
		
		// handle cd
		// cd handling is not ideal, BUT the xv6 shell is JUST AS BAD (it does not support ';')
		if (str_length(input) > 1 && input[0] == 'c' && input[1] == 'd')
		{
			char *dir = str_substr_after(input, "cd ", 0);
			if (chdir(dir) < 0)
				printf("cannot cd %s\n", dir);
			free(dir);
			free(input);
			continue;
		}
		// handle exit
		else if (str_same(str_substr(input, 0, 4), "exit")) // another leak
		{
			free(input);
			break;
		}
		
		// regular recursive input handle
		int f = fork();
		if (f == 0)
			handle_input(input);
		else if (f < 0)
			printf("Fork error!\n");
		wait(0);

		free(input);
	}
	
	exit(0);
}
