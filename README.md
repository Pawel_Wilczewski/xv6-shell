# xv6-shell

A shell script created for one of the university assignments. It suports the basic commands like `cd` and executing scripts, input/output redirection using `>` and `<`, pipes `|` as well as any combination of thereof like `ls | head -3 | tail -1 > output; cd xyz`.
